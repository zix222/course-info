# CSE 262 - Bulletin Board

## :computer: Homework

- [ ] 08/31 - [Homework 1](https://gitlab.com/lehigh-cse262/fall-2020/assignments/homework-1) - Guessing game in Rust
- [ ] 08/31 - [Homework 0](https://gitlab.com/lehigh-cse262/fall-2020/course-info/-/blob/master/Homework-0.md) - Sign up for Gitlab

## :checkered_flag: Quizzes and Exams

## :books: Readings

### Week 1

- [The Missing Semester](https://missing.csail.mit.edu) - Chapters 1, 2, 5, 6
- [RustConf 2020 Keynote](https://youtu.be/ESXMg9OzWrQ?t=1246) - [Slides](https://docs.google.com/presentation/d/e/2PACX-1vSA_hS_o_sOgosYSbT5MnasFBSYxTLCJWjjTX8lqoKm5P8AqAp9wSIa9uYzfd60yFrm1DCjU_dI3AxC/pub)
- [Rust for Non Systems Programmers](https://youtu.be/ESXMg9OzWrQ?t=23842) - [Slides](https://becca.ooo/rustconf/2020/)
- [The Rust Book](https://doc.rust-lang.org/stable/book/ch01-00-getting-started.html) - Chapters 1, 2

## :vhs: Lectures - [Playlist](https://www.youtube.com/playlist?list=PL4A2v89SXU3Trn3mBoocRdPwDULMyk_id)

- Lecture 02 - 08/26 - Introduction to Rust and Course Tools - [Video](https://youtu.be/0zIx9MqDu88) - [Slides](https://gitlab.com/lehigh-cse262/fall-2020/course-info/-/raw/master/slides/CSE%20262%20%E2%80%93%20Lecture%202.pptx)
- Lecture 01 - 08/24 - Course Introduction - [Video](https://youtu.be/vxu-H0QNQ88)